# markrgba-css

一款语义化的CSS基础库

-   声明：
    >    本库并无多少技术含量，仅由个人使用习惯而设计，请根据自身项目酌情使用。

-   特性：
    1. 样式属性语义化为ClassName，实现样式解耦，自由组合。
    2. 可根据业务需求自行维护，打造属于自己的css基础库
    3. 支持按需引入，减小项目体积
    4. 提供[css批量生成工具](https://www.markrgba.cn/#/make-css)，快速生成CSS代码

### 详细文档传送门[markrgba-css](https://docs.markrgba.cn/markrgbaCss/pc/#/introduce)

### 安装

- 在工程中安装：

```bash
npm i markrgba-css-rpx 
```
- 在main.js中引入：   

```js
//全部引入
import './node_modules/markrgba-css-rpx/index.css'
```
- 按需引入：

```js
//按需引入
import './node_modules/markrgba-css-rpx/css/layout.css' //只引入布局
import './node_modules/markrgba-css-rpx/css/font.css' //只引入文字
import './node_modules/markrgba-css-rpx/css/color.css' //只引入颜色
import './node_modules/markrgba-css-rpx/css/margin.css' //只引入外边距
import './node_modules/markrgba-css-rpx/css/padding.css' //只引入内边距
import './node_modules/markrgba-css-rpx/css/border.css' //只引入边框
import './node_modules/markrgba-css-rpx/css/other.css' //只引入其它
```


### 使用

- 在你的vue文件中

```html
<view class="p-all-32">
    <view class="c-999 fs-32 pl-32">
        一款语义化的CSS基础库
    </view>
</view>
```
### 原理解析

- 我们先来看一个`内联样式`的例子：

```html
<view>
    <view style="width:500rpx;height:250rpx;padding-top:20rpx;font-size:28rpx border-radius:20rpx">
        一个box
    </view>
</view>
```

- 内联样式的好处是自由修改，无需去翻找className对应的样式。但，过多的内联样式会让Html非常臃肿。我们来看看markrgba-css干了什么:

```html
<view>
	<view class="w-500 h-250 pt-20 fs-28 br-20">一个box</view>
</view>
```
- 可见，markrgba-css做的工作很简单，`将样式属性抽离为语义化的className`，继承了内联样式的方便，可`自由组合`，同时精简代码。但这么做并不是一劳永逸的，如果遇上元素重复使用的情况，弊端显现，如下：

```html
<view>
    <view class="w-500 h-250 pt-20 fs-28 br-20">一个box</view>
    <view class="w-500 h-250 pt-20 fs-28 br-20">一个box</view>
    <view class="w-500 h-250 pt-20 fs-28 br-20">一个box</view>
    <view class="w-500 h-250 pt-20 fs-28 br-20">一个box</view>
    <view class="w-500 h-250 pt-20 fs-28 br-20">一个box</view>
</view>
```
- 还是不够简洁，怎么办？解决办法就是将`重复使用`的className组合为字符串变量。如下：

```html
<view>
    <view :class="c_box">一个box</view>
    <view :class="c_box">一个box</view>
    <view :class="c_box">一个box</view>
    <view :class="c_box">一个box</view>
    <view :class="c_box">一个box</view>
</view>
```
```js
//======局部抽离=======
export default {
  data() {
    return {
		c_box:'w-500 h-250 pt-20 fs-28 br-20'
	}
  }
}


//======全局抽离=====

//main.js 
Vue.prototype.c_box='w-500 h-250 pt-20 fs-28 br-20'
```

- 建议在`样式已定型`，`不需要频繁修改`，且`超过5个className组合`的情况下再行抽离，否则就失去了本css库的设计初衷。为避免与其它变量冲突，变量命名前缀约定为`c_`，详细文档传送门[markrgba-css](https://docs.markrgba.cn/markrgbaCss/pc/#/introduce)

>  



