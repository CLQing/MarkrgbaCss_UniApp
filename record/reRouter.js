import router from'@/router'

export const optionFt = function(pathName,isBack=false){
	if(isBack){
		router.replace({name:'index'})
	}else{
		router.push({name:pathName})
	}
	window.parent.postMessage({func:'routerTo',router:pathName,isBack:isBack}, '*');
}