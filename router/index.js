import Vue from 'vue'
import Router from 'uni-simple-router'

Vue.use(Router)

const router = new Router({
	encodeURI: false,
	loading: false,
	routes: [{
			path: '/pages/index/index',
			name: 'index',
			aliasPath: '/'
		},
		{
			path: '/pages/business/comcolor',
			name: 'comcolor',
			aliasPath: '/comcolor'
		},
		{
			path: '/pages/child/font',
			name: 'font',
			aliasPath: '/font'
		},
		{
			path: '/pages/child/color',
			name: 'color',
			aliasPath: '/color'
		},
		{
			path: '/pages/child/bcakground',
			name: 'bcakground',
			aliasPath: '/bcakground'
		},
		{
			path: '/pages/child/layout',
			name: 'layout',
			aliasPath: '/layout'
		},
		{
			path: '/pages/child/margin',
			name: 'margin',
			aliasPath: '/margin'
		},
		{
			path: '/pages/child/padding',
			name: 'padding',
			aliasPath: '/padding'
		},
		{
			path: '/pages/child/border',
			name: 'border',
			aliasPath: '/border'
		},
		{
			path: '/pages/child/other',
			name: 'other',
			aliasPath: '/other'
		},
	]
});

//全局路由前置守卫
router.beforeEach((to, from, next) => {
	console.log('to', to)
	next()
})
// 全局路由后置守卫
router.afterEach((to, from) => {})
export default router;
