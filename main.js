import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false
import router from'./router/index.js'
import { RouterMount } from 'uni-simple-router'
import { optionFt } from './record/reRouter.js'
import navbar from './components/navbar.vue'
import pagebg from './components/pagebg.vue'
Vue.component('navbar',navbar)
Vue.component('pagebg',pagebg)
Vue.prototype.$reRouter=optionFt

/* 引入markrgbaCss */
import './node_modules/markrgba-css-rpx/index.css'
//全局组合样式
Vue.prototype.c_card='p-all-32 br-20 bg-fff c-666 fs-32 mt-32'
Vue.prototype.c_strong='bg-f1 fw-b mr-10 c-red'
Vue.prototype.c_title1='fs-34 p-20-0 fw-b'

App.mpType = 'app'

const app = new Vue({
    ...App
})
//v1.3.5起 H5端 你应该去除原有的app.$mount();使用路由自带的渲染方式
// #ifdef H5
	RouterMount(app,'#app');
// #endif

// #ifndef H5
	app.$mount(); //为了兼容小程序及app端必须这样写才有效果
// #endif
